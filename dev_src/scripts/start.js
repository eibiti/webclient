const logger = require('../logger')

logger.info('Starting server...')
require('../serverApp').listen(3000, () => {
  logger.success('Server is running at http://localhost:3000')
})
