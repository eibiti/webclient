import { routerReducer } from 'react-router-redux'
import exemplo from './exemplo'

export default {
  exemplo,
  router: routerReducer,
}
