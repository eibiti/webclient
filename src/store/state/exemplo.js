// Como eh muito simples, pode ficar em um arquivo soh
// Se achar que esta ficando muito complexo pode criar
// uma pasta exemplo/ e quebrar em 2 ou mais arquivos

import createActionTypes from '../utils/createActionTypes'

// Thunks
// ------------------------------------------------------
export const loadPage = (page_number, reload) => (
  async (dispatch, getState) => {
    // simula um retorno da api
    const fetchApi = async () => ({
      number: page_number,
      url: 'http://i8.mangareader.net/kuroshitsuji/49/kuroshitsuji-1493736.jpg',
    })
    const state = getState().exemplo
    if (reload || state.loaded_pages[page_number] === undefined) {
      const page = await fetchApi(`/page/${page_number}`)
      dispatch(addLoadedPage(page))
    }
  }
)

// Action creators
// ------------------------------------------------------
const actionTypes = createActionTypes('exemplo', [
  'ADD_LOADED_PAGE',
  'SET_PAGE_NUMBER',
])

export const setCurrentPageNumber = (page_number) => ({
  type: actionTypes.SET_PAGE_NUMBER,
  page_number,
})

// nao eh exportado porque eh pra ser executado pelo thunk loadPage soh
const addLoadedPage = (page) => ({
  type: actionTypes.ADD_LOADED_PAGE,
  page,
})

// Reducer
// ------------------------------------------------------
const initialState = {
  loaded_pages: {},
  current_page_number: 0,
  page_count: 20,
}

export default (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.ADD_LOADED_PAGE:
      return {
        ...state,
        loaded_pages: {
          ...state.loaded_pages,
          [action.page.number]: action.page,
        },
      }

    case actionTypes.SET_PAGE_NUMBER:
      return {
        ...state,
        current_page_number: action.page_number,
      }
  }
  return state
}
