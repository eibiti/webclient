import reducers from '../state'
import { combineReducers } from 'redux'

const makeRootReducer = (asyncReducers = {}) => {
  return combineReducers({
    ...reducers,
    ...asyncReducers,
  })
}

export default makeRootReducer
