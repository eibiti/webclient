/**
 * Receives a prefix and an array of action types and returns an object, such
 * that each key is a type and its value is `@/${prefix}/${type}`.
 */
const createActionTypes = (prefix, types) => {
  const typesObj = Object.create(null)
  for (const type of types) {
    typesObj[type] = `@@${prefix}/${type}`
  }
  return typesObj
}

export default createActionTypes
