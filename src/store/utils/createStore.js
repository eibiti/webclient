import { applyMiddleware, compose, createStore as createReduxStore } from 'redux'
import thunkMiddleware from 'redux-thunk'
import { routerMiddleware } from 'react-router-redux'

import getHistory from './getHistory'
import makeRootReducer from './makeRootReducer'

const createStore = (initialState = {}) => {

  // Middleware Configuration
  // ------------------------------------------------------
  const middleware = applyMiddleware(
    thunkMiddleware,
    routerMiddleware(getHistory()),
  )

  // Store Enhancers
  // ------------------------------------------------------
  const enhancers = []
  let composeEnhancers = compose

  if (__DEV__) {
    if (typeof window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ === 'function') {
      composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__
    }
  }

  // Store Instantiation and HMR Setup
  // ------------------------------------------------------
  const store = createReduxStore(
    makeRootReducer(),
    initialState,
    composeEnhancers(
      middleware,
      ...enhancers
    )
  )
  store.asyncReducers = {}

  if (module.hot) {
    module.hot.accept('./makeRootReducer', () => {
      const makeRootReducer = require('./makeRootReducer').default
      store.replaceReducer(makeRootReducer(store.asyncReducers))
    })
  }

  return store
}

export default createStore
