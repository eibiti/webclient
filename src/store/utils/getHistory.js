import createHistory from 'history/createBrowserHistory'
import memoize from 'memoizee'

const getHistory = memoize(createHistory)

export default getHistory
