import Reader from './Reader'
import {connect} from 'react-redux'
import {loadPage, setCurrentPageNumber} from '../../store/state/exemplo'

function mapStateToProps(state) {
  console.log(JSON.stringify(state))
  return {
    currentPage: state.exemplo.loaded_pages[state.exemplo.current_page_number],
    pageCount: state.exemplo.page_count,
  }
}

function mapDispatchToProps(dispatch) {
  return {
    setPageNumber: (page_number) => {
      dispatch(loadPage(page_number))
      dispatch(setCurrentPageNumber(page_number))
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Reader)
