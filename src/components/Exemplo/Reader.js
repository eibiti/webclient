import React from 'react'

export default class Reader extends React.Component {
  componentWillMount() {
    this.props.setPageNumber(1)
  }

  render() {
    const {currentPage, pageCount, setPageNumber} = this.props
    const goBack = () => setPageNumber(Math.max(1, currentPage.number - 1))
    const goForward = () => setPageNumber(Math.min(pageCount, currentPage.number + 1))

    return (
      <div>
        {currentPage
          ? <div>
              <button onClick={goBack}>Previous page</button>
              {currentPage.number}
              <button onClick={goForward}>Next page</button>
              <p><img src={currentPage.url} /></p>
            </div>
          : <span>Loading...</span>
        }
      </div>
    )
  }
}
