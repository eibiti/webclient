import React from 'react'

const Alert = (props) => {
  let classes = ["alert", props.className]

  if (props.context)
    classes.push(`alert-${props.context}`)

  const className = classes.filter((e) => e).join(" ")

  return (
    <div className={className}>{props.children}</div>
  )
}

export default Alert