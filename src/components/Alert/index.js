import Alert       from './Alert'
import AlertHeader from './AlertHeader'
import AlertRuler  from './AlertRuler'
import AlertBody   from './AlertBody'
import AlertLink   from './AlertLink'

Alert.Header = AlertHeader
Alert.Ruler  = AlertRuler
Alert.Body   = AlertBody
Alert.Link   = AlertLink

export default Alert