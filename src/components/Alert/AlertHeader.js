import React from 'react'

const AlertHeader = (props) => {
  let classes = ["alert-heading", props.className]

  const className = classes.filter((e) => e).join(" ")

  return (
    <h4 className={className}>{props.children}</h4>
  )
}

export default AlertHeader