import React from 'react'

const AlertLink = (props) => {
  let classes = ["alert-link", props.className]

  const className = classes.filter((e) => e).join(" ")

  return (
    <a href={props.href} className={className}>
      {props.children}
    </a>
  )
}

export default AlertLink