import React from 'react'

const AlertBody = (props) => {
  return (
    <p className={props.className}>
      {props.children}
    </p>
  )
}

export default AlertBody