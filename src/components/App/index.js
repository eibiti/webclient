import React from 'react'
import PropTypes from 'prop-types'
import { Route } from 'react-router-dom'

import './index.scss'
import Home from './routes/Home'

export default () => (
  <div>
    {/*
      Poderia colocar os <Route> todos aqui em vez de em Home, mas em
      alguns casos eh mais interessante colocar no componente mesmo
      porque ele pode precisar renderizar varias routes/subroutes
    */}
    <Route path="/" component={Home} />
  </div>
)
