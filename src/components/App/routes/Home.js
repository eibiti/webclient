import React from 'react'
import { Route, Link } from 'react-router-dom'

import Exemplo from '../../Exemplo'
import Alert from '../../Alert'

export default () => (
  <div>
    <Link to="/">Eibiti</Link>
    <br />
    <Alert context="danger">
      <Alert.Header>Ola</Alert.Header>
      <Alert.Ruler />
      <Alert.Body>
        Este é o <Alert.Link href="/outro-exemplo">link</Alert.Link> para a página principal.
      </Alert.Body>
    </Alert>

    {/* Pagina inicial
      o ideal eh a rota toda estar em um soh componente, mas
      aqui eh soh um exemplo
    */}
    <Route exact path="/" render={() =>
      <Link to="/outro-exemplo">Ir para outro-exemplo</Link>
    } />
    <Route exact path="/" component={Exemplo} />

    {/* Pagina outro-exemplo */}
    <Route exact path="/outro-exemplo" render={() =>
      <span>WOW! SUCH EXEMPLO!!</span>
    } />
  </div>
)
