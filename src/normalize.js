/**
  Browser Normalizer

  This file is responsible for normalizing the browser environment before
  the application starts. Doing this allows us to safely use modern language
  features even when the end user is running an older browser.
*/

// Object.assign
Object.assign = require('object-assign')

// Promise
if (typeof Promise === 'undefined') {
  require('promise/lib/rejection-tracking').enable()
  window.Promise = require('promise/lib/es6-extensions.js')
}

// fetch
if (typeof window.fetch === 'undefined') {
  require('whatwg-fetch')
}

// asap
if (typeof window.asap !== 'function') {
  window.asap = require('asap')
}
